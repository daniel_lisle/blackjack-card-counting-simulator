package project;

/**
 *
 * @author Dan
 */
public class Card implements Comparable {
    
    public enum Rank {

        TWO(2), THREE(3), FOUR(4), FIVE(5), SIX(6), SEVEN(7), EIGHT(8), NINE(9), TEN(10), JACK(11), QUEEN(12), KING(13), ACE(14);
        final int value;

        Rank(int x) {
            value = x;
        }

        public int getValue() {
            return value;
        }
    }

    public enum Suit {

        HEARTS(0), DIAMONDS(1), SPADES(2), CLUBS(3);
        final int value;

        Suit(int x) {
            value = x;
        }

        public int getValue() {
            return value;
        }
    }
    
    private Rank rank;
    private Suit suit;

    public Card(Rank rank, Suit suit) {
        this.rank = rank;
        this.suit = suit;
    }

    public Rank getRank() {
        return rank;
    }

    public void setRank(Rank rank) {
        this.rank = rank;
    }

    public Suit getSuit() {
        return suit;
    }

    public void setSuit(Suit suit) {
        this.suit = suit;
    }

    @Override
    public String toString() {
        return rank + " of " + suit;
    }
    
    @Override
    public int compareTo(Object o) {
        return this.rank.getValue() - ((Card) o).rank.getValue();
    }
    
}
