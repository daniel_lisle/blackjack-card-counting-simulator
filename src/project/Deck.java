package project;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author Dan
 */
public class Deck {

    private ArrayList<Card> deck;

    public Deck() {

        deck = new ArrayList<>();

        for (int i = 0; i < Project.numOfDecks; i++) {
            for (Card.Rank r : Card.Rank.values()) {
                for (Card.Suit s : Card.Suit.values()) {
                    deck.add(new Card(r, s));
                }
            }
        }

        Collections.shuffle(deck);

    }

    public void shuffle() {
        Collections.shuffle(deck);
    }

    public Card deal() {
        return deck.remove(0);
    }

    public int size() {
        return deck.size();
    }

    public void newDeck() {
        int size = deck.size();
        for (int i = 0; i < size; i++) {
            deck.remove(0);
        }

        for (int i = 0; i < Project.numOfDecks; i++) {
            for (Card.Rank r : Card.Rank.values()) {
                for (Card.Suit s : Card.Suit.values()) {
                    deck.add(new Card(r, s));
                }
            }
        }

        Collections.shuffle(deck);
    }
}
