package project;

//split
import java.util.ArrayList;

/*

 */
/**
 *
 * @author Dan
 */
public class Player {

    private double balance = 0;
    private int counting = Project.startCount;
    private long truecount;
    private Card upcard;
    private boolean split;
    private ArrayList<Hand> hands;
    private int wins = 0;
    private int loss = 0;
    private boolean insurance = false;
    private int insuranceBet = 0;

    Player() {
        hands = new ArrayList<>();
        hands.add(new Hand());
    }

    public void resetHands() {
        hands.clear();
        hands.add(new Hand());
    }

    /* Old code to play with Mimic the dealer
     public boolean hit() {
     //If the player has less than 17 they will hit
     if (hand.handValue() < 17) {
     return true;
     }
     return false;
     }
     */
    //Hit = 1, Stick = 2, Double down = 3, Split = 4
    public int basicStSplit(int h) {

        /*
         * -------------------------------------------------------------------
         * The following code is for specific senarios involving the truecount
         * and the use of illustrious 18
         * -------------------------------------------------------------------
         */

        if (Project.cardcounting == true) {

            //order 1
            if (hands.get(h).handValue() == 16 && truecount <= 0 && upcard.getRank().getValue() >= 10 && upcard.getRank().getValue() <= 13) {
                return 2;

                //order 2
            } else if (hands.get(h).handValue() == 15 && truecount >= 4 && upcard.getRank().getValue() >= 10 && upcard.getRank().getValue() <= 13) {
                return 2;

                //order 3
            } else if (hands.get(h).size() == 2 && hands.get(h).getCard(0).getRank().getValue() >= 10 && hands.get(h).getCard(0).getRank().getValue() <= 13
                    && hands.get(h).getCard(1).getRank().getValue() >= 10 && hands.get(h).getCard(1).getRank().getValue() <= 13
                    && upcard.getRank().getValue() == 5 && truecount >= 5) {

                return 2;

                //order 4
            } else if (hands.get(h).size() == 2 && hands.get(h).getCard(0).getRank().getValue() >= 10 && hands.get(h).getCard(0).getRank().getValue() <= 13
                    && hands.get(h).getCard(1).getRank().getValue() >= 10 && hands.get(h).getCard(1).getRank().getValue() <= 13
                    && upcard.getRank().getValue() == 6 && truecount >= 4) {

                return 2;

                //order 5
            } else if (hands.get(h).handValue() == 10 && truecount >= 4 && upcard.getRank().getValue() >= 10 && upcard.getRank().getValue() >= 13) {

                return 2;

                //order 6
            } else if (hands.get(h).handValue() == 12 && truecount >= 2 && upcard.getRank().getValue() == 3) {

                return 2;

                //order 7
            } else if (hands.get(h).handValue() == 12 && truecount >= 3 && upcard.getRank().getValue() == 3) {

                return 2;

                //order 8
            } else if (hands.get(h).handValue() == 11 && truecount >= 1 && upcard.getRank().getValue() == 14) {

                return 2;

                //order 9
            } else if (hands.get(h).handValue() == 2 && truecount >= 1 && upcard.getRank().getValue() == 2) {

                return 2;

                //order 10
            } else if (hands.get(h).handValue() == 10 && truecount >= 4 && upcard.getRank().getValue() == 14) {

                return 2;

                //order 11            
            } else if (hands.get(h).handValue() == 9 && truecount >= 3 && upcard.getRank().getValue() == 7) {

                return 2;

                //order 12
            } else if (hands.get(h).handValue() == 16 && truecount >= 5 && upcard.getRank().getValue() == 9) {

                return 2;

                //order 13
            } else if (hands.get(h).handValue() == 13 && truecount <= -1 && upcard.getRank().getValue() == 2) {

                return 1;

                //order 14
            } else if (hands.get(h).handValue() == 12 && truecount <= 0 && upcard.getRank().getValue() == 4) {

                return 1;

                //order 15
            } else if (hands.get(h).handValue() == 12 && truecount <= -2 && upcard.getRank().getValue() == 5) {

                return 1;

                //order 16
            } else if (hands.get(h).handValue() == 12 && truecount <= -1 && upcard.getRank().getValue() == 6) {

                return 1;

                //order 17
            } else if (hands.get(h).handValue() == 13 && truecount <= -2 && upcard.getRank().getValue() == 3) {

                return 1;

                //order 18
            }

        }


        /*
         * -------------------------------------------------------------------
         * This code below is all for basic stratergy
         * -------------------------------------------------------------------
         */

        //If the player has blackjack they should stick
        if (hands.get(h).handValue() == 21) {
            return 2;
        }

        //If the player hasn't already split they can choose to do so.
        if (!split) {
            //If both cards are the same and the player has only 2 cards they can split
            if (hands.get(h).getCard(0).getRank().value == hands.get(h).getCard(1).getRank().value && hands.get(h).size() == 2 && !split) {

                //Pair of aces
                if (hands.get(h).searchHand(14)) {
                    return 4;

                    //Pair of 2s
                } else if (hands.get(h).searchHand(2)) {

                    if (upcard.getRank().value <= 7 && upcard.getRank().value >= 3) {

                        return 4;

                    }

                    return 1;

                    //Pair of 3s
                } else if (hands.get(h).searchHand(3)) {

                    if (upcard.getRank().value <= 7 && upcard.getRank().value >= 4) {

                        return 4;

                    }

                    return 1;

                    //Pair of 4s
                } else if (hands.get(h).searchHand(4)) {

                    if (upcard.getRank().value == 5 && upcard.getRank().value == 6) {

                        return 3;

                    }

                    return 1;

                    //Pair of 5s
                } else if (hands.get(h).searchHand(5)) {

                    if (upcard.getRank().value <= 9) {

                        return 3;

                    }

                    return 1;

                    //Pair of 6s
                } else if (hands.get(h).searchHand(6)) {

                    if (upcard.getRank().value <= 6) {

                        return 4;

                    }

                    return 1;

                    //Pair of 7s
                } else if (hands.get(h).searchHand(7)) {

                    if (upcard.getRank().value <= 7) {

                        return 4;

                    } else if (upcard.getRank().value < 14 && upcard.getRank().value >= 10) {

                        return 2;

                    }

                    return 1;

                    //Pair of 8s
                } else if (hands.get(h).searchHand(8)) {

                    return 4;

                    //Pair of 9s
                } else if (hands.get(h).searchHand(9)) {

                    if (upcard.getRank().value == 7 || upcard.getRank().value >= 10) {

                        return 2;
                    }

                    return 4;

                    //Cards with value Ten
                } else {
                    return 2;
                }
            }
        }

        //Had which includes an ace
        if (hands.get(h).searchHand(14) && hands.get(h).isSoft()) {

            //Spliting aces
            if (hands.get(h).handValue() == 11) {
                return 1;

                //Ace and vcalue of 2 3 4 or 5
            } else if (hands.get(h).handValue() == 13 || hands.get(h).handValue() == 14 || hands.get(h).handValue() == 15 || hands.get(h).handValue() == 16) {

                //Up card is 4 5 or 6
                if (upcard.getRank().value < 7 && upcard.getRank().value > 3 && hands.get(h).size() == 2) {

                    //Player is Doubling down
                    return 3;

                }

                //Player is hitting
                return 1;

                //Ace and value of 6
            } else if (hands.get(h).handValue() == 17) {

                //Upcard is 2 3 4 5 or 6
                if (upcard.getRank().value < 7 && hands.get(h).size() == 2) {

                    //Player doubles down
                    return 3;

                }

                //Player stands
                return 2;

                //Ace  and value of 7
            } else if (hands.get(h).handValue() == 18) {

                //Upcard is 3 4 5 or 6
                if (upcard.getRank().value < 7 && upcard.getRank().value >= 3 && hands.get(h).size() == 2) {

                    //Player doubles down
                    return 3;

                    //Upcard is 9 or a card value 10
                } else if (upcard.getRank().value <= 13 && upcard.getRank().value >= 9) {

                    //Player hits
                    return 1;

                }

                //Player stands
                return 2;

                //Ace 8
            } else if (hands.get(h).handValue() == 19) {

                //Upcard is 6
                if (upcard.getRank().value == 6 && hands.get(h).size() == 2) {

                    //Player doubles down
                    return 3;

                }

                //Player stands
                return 2;

            }

            //Hand sums to 8
        } else if (hands.get(h).handValue() < 8) {
            return 1;
        } else if (hands.get(h).handValue() == 8) {

            //Upcard is 5or 6
            if (upcard.getRank().value == 5 || upcard.getRank().value == 6 && hands.get(h).size() == 2) {

                //Player doubles down
                return 3;

            }

            //Player hits
            return 1;

            //Hand sums to 9
        } else if (hands.get(h).handValue() == 9) {

            //Upcard is 2 3 4 5 or 6
            if (upcard.getRank().value <= 6 && hands.get(h).size() == 2) {

                //Player doubles down
                return 3;

            }

            //Player hits
            return 1;

            //Hand sums to 10
        } else if (hands.get(h).handValue() == 10) {

            //Upcard is 2 3 4 5 6 7 8 or 9
            if (upcard.getRank().value <= 9 && hands.get(h).size() == 2) {

                //Player doubles down
                return 3;

            }

            //player hits
            return 1;

            //hand sums to 11
        } else if (hands.get(h).handValue() == 11 && hands.get(h).size() == 2) {

            //Player doubles down
            return 3;

            //hand sums to 12
        } else if (hands.get(h).handValue() == 12) {

            //Upcard is 4 5 or 6
            if (upcard.getRank().value == 4 || upcard.getRank().value == 5 || upcard.getRank().value == 6) {

                //Player stands
                return 2;

            }

            //Player hits
            return 1;

            //hand sums to 13 14 15 or 16
        } else if (hands.get(h).handValue() >= 13 && hands.get(h).handValue() <= 16) {

            //upcard is 2 3 4 5 or 6
            if (upcard.getRank().value < 7) {

                //Player stands
                return 2;

            }

            //player hits
            return 1;
        }
        //If it doesn't meet any of the above situations then the player must be sticking
        return 2;
    }

    public int bet(int minBet) {

        if (truecount >= 2) {

            hands.get(0).setBet(truecount * 25);
            
            balance = balance - (truecount * 25);

            
        } else {

            //Set's bet for the hand
            hands.get(0).setBet(25);

            //Reduces balance
            balance = balance - 5;


        }

        //Returns bet
        return hands.get(0).getBet();

    }

    //Add's a card to the players specific hand
    public void addCard(Card c, int h) {
        hands.get(h).addCard(c);
    }

    public void settleBet(int p, int h) {
        switch (p) {

            //The player lost so loses thier money
            case -1:
                hands.get(h).setBet(0);
                loss++;
                break;

            //Player and dealer had blackjack so bet returned or tied
            case 1:
                this.balance = this.balance + hands.get(h).getBet();
                hands.get(h).setBet(0);
                loss++;
                break;

            //Player won so twice bet returned
            case 2:
                this.balance = this.balance + hands.get(h).getBet() * 2;
                hands.get(h).setBet(0);
                wins++;
                break;

            //Player beat dealer with blackjack so recieves 3:2 return
            case 3:
                this.balance = this.balance + hands.get(h).getBet() * 2.5;
                hands.get(h).setBet(0);
                wins++;
                break;

        }
    }

    public void cardCount(Card c, double decksRemaining) {
        int compare = c.getRank().getValue();

        if (Project.cardcounting == true) {

            //curently counting with HiLo
            switch (compare) {
                //Ace
                case 14:
                    break;
                case 2:
                    
                    /*
                    //Red2
                    if(c.getSuit().value == 0 || c.getSuit().value == 1){
                        
                    }
                    //Black2
                    else {
                        counting++;
                    }
                    */
                    counting++;
                    break;
                case 3:
                    counting++;
                    counting++;
                    break;
                case 4:
                    counting++;
                    counting++;
                    break;
                case 5:
                    counting++;
                    counting++;
                    counting++;
                    break;
                case 6:
                    counting++;
                    counting++;
                    break;
                case 7:
                    /*
                    //Red7
                    if(c.getSuit().value == 0 || c.getSuit().value == 1){
                        
                    }
                    //Black7
                    else{
                        counting++;
                    }
                    */
                    
                    break;
                case 8:
                    counting++;
                    break;
                case 9:
                    counting--;
                    break;
                case 10:
                    counting--;counting--;counting--;
                    break;
                //Jack
                case 11:
                    counting--;counting--;counting--;
                    break;
                //Queen
                case 12:
                    counting--;counting--;counting--;
                    break;
                //King
                case 13:
                    counting--;counting--;counting--;
                    break;
            }

            if(Project.balanced){
                
                
                int dksRemain;
            if (decksRemaining >= 7.5) {
                dksRemain = 8;
            } else if (decksRemaining < 7.5 && decksRemaining >= 6.5) {
                dksRemain = 7;
            } else if (decksRemaining < 6.5 && decksRemaining >= 5.5) {
                dksRemain = 6;
            } else if (decksRemaining < 5.5 && decksRemaining >= 4.5) {
                dksRemain = 5;
            } else if (decksRemaining < 4.5 && decksRemaining >= 3.5) {
                dksRemain = 4;
            } else if (decksRemaining < 3.5 && decksRemaining >= 2.5) {
                dksRemain = 3;
            } else if (decksRemaining < 2.5 && decksRemaining >= 1.5) {
                dksRemain = 2;
            } else {
                dksRemain = 1;
            }
                
            
            truecount = counting / dksRemain;
            
            } else {
                truecount = counting;
            }

        }
    }

    public int handValue(int h) {
        return hands.get(h).handValue();
    }

    public boolean checkBlackjack(int h) {
        return hands.get(h).checkBlackjack();
    }

    public boolean bust(int h) {
        if (hands.get(h).handValue() > 21) {
            return true;
        }
        return false;
    }

    public void splitHand() {

        split = true;
        hands.add(new Hand());
        hands.get(1).setBet(hands.get(0).getBet());
        this.balance = this.balance - hands.get(0).getBet();
        hands.get(1).addCard(hands.get(0).removeCard());


    }

    public void checkInsurance() {

        if (this.upcard.getRank().getValue() == 13 && this.counting >= 3) {
            this.insuranceBet = (hands.get(0).getBet()) / 2;
            this.balance = this.balance - (hands.get(0).getBet()) / 2;
            this.insurance = true;
        }

    }

    public void settleInsurance() {
        this.balance = this.balance + (this.insuranceBet * 2);
        this.insuranceBet = 0;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public int getBet(int h) {
        return hands.get(h).getBet();
    }

    public void setBet(int bet, int h) {
        hands.get(h).setBet(bet);
    }

    public Hand getHand(int h) {
        return hands.get(h);
    }

    public Card getUpcard() {
        return upcard;
    }

    public void setUpcard(Card upcard) {
        this.upcard = upcard;
    }

    public int getCounting() {
        return counting;
    }

    public void setCounting(int counting) {
        this.counting = counting;
    }

    public long getTruecount() {
        return truecount;
    }

    public void setTruecount(long truecount) {
        this.truecount = truecount;
    }

    public boolean isSplit() {
        return split;
    }

    public void setSplit(boolean split) {
        this.split = split;
    }

    public ArrayList<Hand> getHands() {
        return hands;
    }

    public void setHands(ArrayList<Hand> hands) {
        this.hands = hands;
    }

    public int handsSize() {
        return hands.size();
    }

    public int getWins() {
        return wins;
    }

    public void setWins(int wins) {
        this.wins = wins;
    }

    public int getLoss() {
        return loss;
    }

    public void setLoss(int loss) {
        this.loss = loss;
    }

    public boolean isInsurance() {
        return insurance;
    }

    public void setInsurance(boolean insurance) {
        this.insurance = insurance;
    }

    public int getInsuranceBet() {
        return insuranceBet;
    }

    public void setInsuranceBet(int insuranceBet) {
        this.insuranceBet = insuranceBet;
    }
}
