package project;

/**
 *
 * @author Dan
 */
public class Dealer {

    private Hand hand;
    private Deck deck;
    private Player p;
    int minbet = 10;

    Dealer() {
        hand = new Hand();
        deck = new Deck();
        p = new Player();
    }

    Dealer(Player player) {
        hand = new Hand();
        deck = new Deck();
        p = player;
    }

    public void checkBet() {
        p.bet(minbet); //Second interger is minimum bet
    }

    public void openingHands() {

        //Create a new deck
        if (deck.size() < 13) {
            deck.newDeck();
            p.setCounting(Project.startCount);
        }

        //Deal opening hands
        for (int i = 0; i < 2; i++) {
            //Deals player a card
            p.getHand(0).addCard(deck.deal());

            //Card count with the delt card
            p.cardCount(p.getHand(0).getCard(p.getHand(0).size() - 1), (double)deck.size()/52);

            //Dealer delt a card
            hand.addCard(deck.deal());

        }

        //Send the upcard to the player
        p.setUpcard(hand.getCard(0));

        //Card count with upcard
        p.cardCount(hand.getCard(0), (double)deck.size()/52);

    }


    /* Old code to play with Mimic the dealer
     public void playPlayer(int h) {

     boolean stick = false;

     while (!p.bust() && !stick && !p.checkBlackjack()) {
     System.out.println("Player " + p.getHand().toString());
     if (p.hit()) {
     p.addCard(deck.deal());
     System.out.println("Player hit " + p.getHand().toString());
     } else {
     stick = true;
     System.out.println("Player sticks " + p.getHand().toString());
     }

     if (p.bust()) {
     System.out.println("Player bust " + p.getHand().toString());
     }
     }
     }
     */
    public void basicStPlay(int h) {
        boolean stick = false;


        while (!p.bust(h) && !stick) {

            //Hit
            if (p.basicStSplit(h) == 1) {
                p.getHand(h).addCard(deck.deal());

                //Card count with the delt card
                p.cardCount(p.getHand(0).getCard(p.getHand(0).size() - 1), (double)deck.size()/52);


                //Stick
            } else if (p.basicStSplit(h) == 2) {
                stick = true;

                //Double down
            } else if (p.basicStSplit(h) == 3) {
                stick = true;

                p.setBalance(p.getBalance() - p.getBet(h));
                p.getHand(h).setBet(p.getBet(h) + p.getBet(h));

                p.getHand(h).addCard(deck.deal());

                //Card count with the delt card
                p.cardCount(p.getHand(0).getCard(p.getHand(0).size() - 1), (double)deck.size()/52);


                //Split
            } else if (p.basicStSplit(h) == 4 && !p.isSplit()) {
                p.splitHand();

            } else {
                System.exit(0);
            }

            if (p.bust(h)) {
            }
        }

    }

    public void playDealer() {
        boolean stick = false;
        boolean bust = false;

        while (!stick && !bust) {
            if (hand.handValue() < 17) {
                hand.addCard(deck.deal());
                
                //Card count with upcard
                p.cardCount(hand.getCard(hand.size()-1), (double)deck.size()/52);
                
                //dealer hits on soft 17
            } else if (hand.handValue() == 17 && hand.searchHand(14) && hand.isSoft()) {
                
                hand.addCard(deck.deal());
                
                //Card count with upcard
                p.cardCount(hand.getCard(hand.size()-1), (double)deck.size()/52);

            } else if (hand.handValue() > 21) {
                bust = true;
            } else {
                stick = true;
            }
        }

    }

    public void settleBets(int h) {
        //Check player isn't bust
        if (!p.bust(h)) {
            //Check if player has blackjack
            if (p.checkBlackjack(h)) {
                if (hand.checkBlackjack()) {
                    //Dealer has blackjack so player gets his bet back
                    p.settleBet(1, h);
                } else {
                    //Dealer didnt have blackjack so player wins with 3:2
                    p.settleBet(3, h);
                }
                //checks dealers hand is less than or equal to 21
            } else if (hand.handValue() <= 21) {
                //Checks if dealers hand is greater than the players
                if (hand.handValue() > p.getHand(h).handValue()) {
                    //Dealer beat player with a higher value
                    p.settleBet(-1, h);
                    //Check is dealer and player tie
                } else if (hand.handValue() == p.getHand(h).handValue()) {
                    p.settleBet(1, h);
                } else {
                    //Player won because they had a higher value
                    p.settleBet(2, h);
                }
            } else {
                //Player won because dealer was bust
                p.settleBet(2, h);
            }
        } else {
            //Player lost because they went bust
            p.settleBet(-1, h);
        }


        /* Method to work out the current count for card counting
         Hand temp = new Hand();
         for (int i = 0; i < p.getHand().size(); i++) {
         temp.addCard(p.getHand().getCard(i));
         }
         for (int i = 0; i < hand.size(); i++) {
         temp.addCard(hand.getCard(i));
         }
        
         for (int i = 0; i < temp.size(); i++) {
         p.cardCount(temp.getCard(i));
         System.out.println("Current Hi-Lo Count is "+p.getCounting());
         }
         */

    }

    public void resetGame() {
        p.resetHands();
        hand = new Hand();
    }

    public Hand getHand() {
        return hand;
    }

    public void setHand(Hand hand) {
        this.hand = hand;
    }

    public Deck getDeck() {
        return deck;
    }

    public void setDeck(Deck deck) {
        this.deck = deck;
    }

    public Player getPlayer() {
        return p;
    }

    public void setPlayer(Player p) {
        this.p = p;
    }
}
