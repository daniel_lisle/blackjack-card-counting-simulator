package project;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author Dan
 */
public class Hand {

    private ArrayList<Card> hand;
    private boolean soft;
    private int bet;

    public Hand() {
        hand = new ArrayList<>();
    }

    public void addCard(Card c) {
        hand.add(c);
    }

    public int size() {
        return hand.size();
    }

    public Card getCard(int x) {
        return hand.get(x);
    }

    public Card getUpcard() {
        return hand.get(1);
    }

    public Card removeCard() {
        return hand.remove(0);
    }

    public boolean checkBlackjack() {

        if (hand.size() == 2) {
            //Checks if first card is an ace
            if (hand.get(0).getRank().equals(Card.Rank.ACE)) {
                //if second card is ten, jack, queen, or jack the player has blackjack
                if (hand.get(1).getRank().equals(Card.Rank.TEN)
                        || hand.get(1).getRank().equals(Card.Rank.JACK)
                        || hand.get(1).getRank().equals(Card.Rank.QUEEN)
                        || hand.get(1).getRank().equals(Card.Rank.KING)) {
                    return true;
                }
                //Checks second card for ace if first card was not ace
            } else if (hand.get(1).getRank().equals(Card.Rank.ACE)) {
                //if first card is ten, jack, queen, or jack the player has blackjack
                if (hand.get(0).getRank().equals(Card.Rank.TEN)
                        || hand.get(0).getRank().equals(Card.Rank.JACK)
                        || hand.get(0).getRank().equals(Card.Rank.QUEEN)
                        || hand.get(0).getRank().equals(Card.Rank.KING)) {
                    return true;
                }
            }
            return false;
        } else {
            return false;
        }
    }

    public int handValue() {
        int total = 0;
        //Place Aces at the end of the Hand since they are a special case
        Collections.sort(hand);

        for (int i = 0; i < hand.size(); i++) {
            //If the rank value is less than 10 the value of the rank is added to total
            if (hand.get(i).getRank().value < 10) {
                total += hand.get(i).getRank().value;
                //If the card is either 10 jack queen or king 10 is added to value
            } else if (hand.get(i).getRank().value < 14 && hand.get(i).getRank().value > 9) {
                total += 10;
            } else {
                //If an ace as 11 will bust the player 1 is added to total
                if (total + 11 > 21) {
                    total++;
                    soft = false;
                    //If adding 11 wont bust the player 11 is added
                } else {
                    total += 11;
                    soft = true;
                }
            }
        }

        return total;
    }

    public boolean isSoft() {
        return soft;
    }

    public void setSoft(boolean isSoft) {
        this.soft = isSoft;
    }

    public boolean searchHand(int card) {
        for (int i = 0; i < this.hand.size(); i++) {
            if (card == this.hand.get(i).getRank().value) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return "Hand is " + hand;
    }

    public int getBet() {
        return bet;
    }

    public void setBet(double bet) {
        this.bet = (int)bet;
    }
}
